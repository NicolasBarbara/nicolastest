//
//  Note.swift
//  NicolasTest
//
//  Created by Nicolas Barbara on 10/30/20.
//

import SwiftyJSON
struct Note {
    let id:String
    let title:String
    var content:String
    let type:Int
    let is_deleted:Bool
    init(_ json:JSON) {
        id = json["id"].stringValue
        title = json["title"].stringValue
        content = json["content"].stringValue
        type = json["type"].intValue
        is_deleted = json["is_deleted"].boolValue
    }
}

