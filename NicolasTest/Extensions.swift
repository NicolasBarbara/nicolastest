//
//  Extensions.swift
//  NicolasTest
//
//  Created by Nicolas Barbara on 10/30/20.
//

import UIKit
extension UIViewController{
    func showAlert(_ title:String){
        let alert = UIAlertController(title: title, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok".localized, style: .cancel))
        
        present(alert, animated: true)
    }
}
extension String{
    var localized: String {
        if let arr = UserDefaults.standard.value(forKey: "AppleLanguages") as? [String] {
            if let lang = arr.first {
                let userLang = lang.contains("ar") ? "ar" : "en"
                let path = Bundle.main.path(forResource: userLang, ofType: "lproj")
                let bundle = Bundle.init(path: path!)
                let localizedString = bundle!.localizedString(forKey: self, value: nil, table: nil)
                return localizedString
            }
        }
        return ""
    }
}
