//
//  AddNoteVC.swift
//  NicolasTest
//
//  Created by Nicolas Barbara on 10/30/20.
//

import SnapKit
class AddNoteVC:UIViewController{
    weak var vc:ViewController?
    let descLabel = UILabel()
    let titleLabel = UILabel()
    let titleTextField = UITextField()
    let contentTextField = UITextField()
    let sg = UISegmentedControl(items: ["Type 1".localized,"Type 2".localized])
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .never
        view.backgroundColor = .systemBackground
        title = "Add Note".localized
        sg.addTarget(self, action: #selector(onChangeSg(sender:)), for: .valueChanged)
        titleLabel.text = "Title".localized
        descLabel.text = "Enter content".localized
        sg.selectedSegmentIndex = 0
        titleLabel.textAlignment = .center
        descLabel.textAlignment = .center
        descLabel.numberOfLines = 0
        contentTextField.placeholder = "content".localized
        titleTextField.placeholder = "title".localized
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(onClickSave))
        view.addSubview(sg)
        view.addSubview(titleLabel)
        view.addSubview(titleTextField)
        view.addSubview(descLabel)
        view.addSubview(contentTextField)
        titleTextField.borderStyle = .roundedRect
        contentTextField.borderStyle = .roundedRect
        sg.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(16)
            make.height.equalTo(30)
            make.width.equalTo(150)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(sg.snp.bottom).offset(8)
            make.height.equalTo(50)
            make.width.equalTo(view.snp.width).offset(-32)
        }
        titleTextField.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(titleLabel.snp.bottom)
            make.height.equalTo(40)
            make.width.equalTo(view.snp.width).offset(-32)
        }
        descLabel.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(titleTextField.snp.bottom)
            make.height.equalTo(72)
            make.width.equalTo(view.snp.width).offset(-32)
        }
        contentTextField.snp.makeConstraints { (make) in
            make.centerX.equalTo(view.snp.centerX)
            make.top.equalTo(descLabel.snp.bottom)
            make.height.equalTo(40)
            make.width.equalTo(view.snp.width).offset(-32)
        }
    }
    @objc func onClickSave(){
        view.endEditing(false)
        guard let title = titleTextField.text , title != "" else{
            showAlert("Please Enter Title".localized)
            return
        }
        guard let content = contentTextField.text , content != "" else{
            showAlert(sg.selectedSegmentIndex == 0 ? "Please Enter Content".localized : "Please Enter Checklist".localized)
            return
        }
        let loadingVC = LoadingVC()
        present(loadingVC, animated: false) {
            Api.sharde.createNoteType1(title: title, content: content,type: self.sg.selectedSegmentIndex == 0 ? 1:2) { [weak self,weak loadingVC](res, err) in
                loadingVC?.dismiss(animated: false, completion: {
                    if let err = err{
                        self?.showAlert(err)
                    }
                    else if let res = res{
                        self?.vc?.list = res
                        self?.vc?.tableView.reloadData()
                        self?.navigationController?.popViewController(animated: true)
                    }
                })
            }
        }
    }
    @objc func onChangeSg(sender:UISegmentedControl){
        if sender.selectedSegmentIndex == 0 {
            descLabel.text = "Enter title and content".localized
            contentTextField.placeholder = "content...".localized
        }
        else{
            descLabel.text = "Enter checklist(separated by , )".localized
            contentTextField.placeholder = "checklist(separated by , )".localized
        }
    }
}
