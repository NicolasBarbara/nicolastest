//
//  EditNoteVC.swift
//  NicolasTest
//
//  Created by Nicolas Barbara on 10/30/20.
//

import SnapKit

class EditNoteVC:AddNoteVC{
    var note:Note!
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Edit Note".localized
        titleTextField.text = note.title
        contentTextField.text = note.content
        if note.type == 2{
            sg.selectedSegmentIndex = 1
        }
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(onClickSave)),
            UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(onClickDelete))
        ]
    }
    override func onClickSave() {
        view.endEditing(false)
        guard let title = titleTextField.text , title != "" else{
            showAlert("Please Enter Title".localized)
            return
        }
        guard let content = contentTextField.text , content != "" else{
            showAlert(sg.selectedSegmentIndex == 0 ? "Please Enter Content".localized : "Please Enter Checklist".localized)
            return
        }
        let loadingVC = LoadingVC()
        present(loadingVC, animated: false) {
            Api.sharde.editNoteWithId(self.note.id,title:title,type: self.sg.selectedSegmentIndex == 0 ? 1:2,content: content) {  [weak self,weak loadingVC](res, err) in
                loadingVC?.dismiss(animated: false, completion: {
                    if let err = err{
                        self?.showAlert(err)
                    }
                    else if let res = res{
                        self?.vc?.list = res
                        self?.vc?.tableView.reloadData()
                        self?.navigationController?.popViewController(animated: true)
                    }
                })
            }
        }
    }
    @objc func onClickDelete(){
        view.endEditing(false)
        let loadingVC = LoadingVC()
        present(loadingVC, animated: false) {
            Api.sharde.deleteNoteWithId(self.note.id,title:self.note.title,type: self.note.type,content: self.note.content) {  [weak self,weak loadingVC](res, err) in
                loadingVC?.dismiss(animated: false, completion: {
                    if let err = err{
                        self?.showAlert(err)
                    }
                    else if let res = res{
                        self?.vc?.list = res
                        self?.vc?.tableView.reloadData()
                        self?.navigationController?.popViewController(animated: true)
                    }
                })
            }
        }
    }
}
