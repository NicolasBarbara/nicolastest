//
//  LoadingVC.swift
//  NicolasTest
//
//  Created by Nicolas Barbara on 10/30/20.
//

import UIKit
class LoadingVC:UIViewController{
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.label.withAlphaComponent(0.5)
        let actitvityIndicator = UIActivityIndicatorView(style: .large)
        actitvityIndicator.color = .systemBackground
        view.addSubview(actitvityIndicator)
        actitvityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            actitvityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            actitvityIndicator.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            actitvityIndicator.widthAnchor.constraint(equalToConstant: 50),
            actitvityIndicator.heightAnchor.constraint(equalToConstant: 50),
        ])
        actitvityIndicator.startAnimating()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
