//
//  CheckListVC.swift
//  NicolasTest
//
//  Created by Nicolas Barbara on 10/30/20.
//

import SnapKit
class CheckListVC:UIViewController,UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Cell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = list[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        if selectetIndexes.contains(indexPath.row){
            cell.accessoryType = .checkmark
        }
        else{
            cell.accessoryType = .none
        }
        return cell
    }
    var selectetIndexes:[Int] = []
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let index = selectetIndexes.firstIndex(of: indexPath.row){
            selectetIndexes.remove(at: index)
        }
        else{
            selectetIndexes.append(indexPath.row)
        }
        tableView.reloadData()
    }
    class Cell:UITableViewCell{
        
    }
    var list = [String]()
    lazy var tableView:UITableView = {
        let t = UITableView()
        t.allowsMultipleSelection = true
        t.dataSource = self
        t.delegate = self
        t.tableFooterView = UIView()
        return t
    }()
    init() {
        super.init(nibName: nil, bundle: nil)
        modalPresentationStyle = .overFullScreen
        modalTransitionStyle = .crossDissolve
    }
    weak var vc:ViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.label.withAlphaComponent(0.5)
        let closeButton = UIButton(type: .system)
        closeButton.addTarget(self, action: #selector(onClickClose), for: .touchUpInside)
        closeButton.setTitle("Close".localized, for: .normal)
        let backView = UIView()
        backView.backgroundColor = .systemBackground
        backView.clipsToBounds = true
        backView.layer.cornerRadius = 8
        view.addSubview(backView)
        let height = min((CGFloat(list.count) * 50) + 44,view.frame.height - 120)
        backView.snp.makeConstraints { (make) in
            make.center.equalTo(view.safeAreaLayoutGuide.snp.center)
            make.width.equalTo(view.safeAreaLayoutGuide.snp.width).multipliedBy(0.8)
            make.height.equalTo(height)
        }
        let titleLabel = UILabel()
        titleLabel.text = "Checklist".localized
        titleLabel.textAlignment = .center
        backView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.top.leading.trailing.equalTo(backView)
            make.height.equalTo(44)
        }
        titleLabel.font = .boldSystemFont(ofSize: 16)
        backView.addSubview(tableView)
        tableView.snp.makeConstraints { (make) in
            make.bottom.leading.trailing.equalTo(backView)
            make.top.equalTo(titleLabel.snp.bottom)
        }
        backView.addSubview(closeButton)
        closeButton.sizeToFit()
        closeButton.snp.makeConstraints { (make) in
            make.top.leading.bottom.equalTo(titleLabel)
            make.width.equalTo(closeButton.frame.width + 20)
        }
        let editButton = UIButton(type: .system)
        editButton.setTitle("Edit".localized, for: .normal)
        editButton.addTarget(self, action: #selector(onClickEdit), for: .touchUpInside)
        backView.addSubview(editButton)
        editButton.sizeToFit()
        editButton.snp.makeConstraints { (make) in
            make.trailing.bottom.top.equalTo(titleLabel)
            make.width.equalTo(editButton.frame.width + 20)
        }
        let line = UIView()
        line.backgroundColor = .label
        backView.addSubview(line)
        line.snp.makeConstraints { (make) in
            make.leading.trailing.bottom.equalTo(titleLabel)
            make.height.equalTo(1)
        }
    }
    var note:Note!
    @objc func onClickEdit(){
        let vc = EditNoteVC()
        vc.note = note
        vc.vc = self.vc
        dismiss(animated: true) {
            self.vc?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @objc func onClickClose(){
        dismiss(animated: true)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
