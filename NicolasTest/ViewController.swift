//
//  ViewController.swift
//  NicolasTest
//
//  Created by Nicolas Barbara on 10/29/20.
//
import SnapKit
class ViewController: UITableViewController, UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        filterData = searchText.isEmpty ? list : list.filter {   $0.title.lowercased().contains(searchText.lowercased()) || $0.content.lowercased().contains(searchText.lowercased()) }
        tableView.reloadData()
    }
    var filterData = [Note]()
    var list = [Note]()
    let searchController = UISearchController(searchResultsController: nil)
    let actitvityIndicator = UIActivityIndicatorView(style: .large)
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.largeTitleDisplayMode = .automatic
        title = "Notes".localized
        tableView.tableFooterView = UIView()
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(onClickAdd))
        getData()
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(getData), for: .valueChanged)
        navigationItem.hidesSearchBarWhenScrolling = true
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        view.addSubview(actitvityIndicator)
        actitvityIndicator.snp.makeConstraints { (make) in
            make.center.equalTo(view.safeAreaLayoutGuide.snp.center)
            make.width.height.equalTo(100)
        }
        actitvityIndicator.startAnimating()
    }
    @objc func getData(){
        searchController.searchBar.text = ""
        Api.sharde.getNotes { [weak self](res,err) in
            guard let self = self else{return}
            self.actitvityIndicator.stopAnimating()
            self.refreshControl?.endRefreshing()
            if let err = err{
                let alert = UIAlertController(title: err, message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok".localized, style: .cancel))
                self.present(alert, animated: true)
            }
            else if let res = res{
                self.list = res
                self.filterData = self.list
                self.tableView.reloadData()
            }
        }
    }
    @objc func onClickAdd(){
        let vc = AddNoteVC()
        vc.vc = self
        navigationController?.pushViewController(vc, animated: true)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        filterData.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = filterData[indexPath.row].title
        cell.detailTextLabel?.text = filterData[indexPath.row].type == 1 ? filterData[indexPath.row].content : nil
        cell.detailTextLabel?.numberOfLines = 0
        cell.textLabel?.numberOfLines = 0
        if list[indexPath.row].type == 2{
            cell.accessoryType = .detailButton
        }
        else{
            cell.accessoryType = .disclosureIndicator
        }
        cell.selectionStyle = .none
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if filterData[indexPath.row].type == 2{
            let vc = CheckListVC()
            vc.vc = self
            vc.note = filterData[indexPath.row]
            vc.list = filterData[indexPath.row].content.components(separatedBy: ",")
            present(vc, animated: true)
        }
        else{
            let vc = EditNoteVC()
            vc.note = filterData[indexPath.row]
            vc.vc = self
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}





