//
//  Api.swift
//  NicolasTest
//
//  Created by Nicolas Barbara on 10/30/20.
//

import Alamofire
import SwiftyJSON
class Api{
    static var sharde = Api()
    func makeRequst(params:[String:Any],completion:@escaping(_ res:[Note]?,_ err:String?)->Void){
        let url = "http://mbtest.ucglab.com/sync_notes.php?user_id=Nicolas"
        var request = URLRequest(url: URL(string:url)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let array:[[String:Any]] = [params]
        request.httpBody = try! JSONSerialization.data(withJSONObject: array)
        if params.isEmpty {
            request.httpBody = try! JSONSerialization.data(withJSONObject: [])
        }
        AF.request(request).responseJSON { (res) in
            switch res.result{
            case .success(let value):
                let json = JSON(value)
                print(json)
                if let err = json["error"].string{
                    completion(nil,err)
                    return
                }
                var r = [Note]()
                if let array = json.array{
                    if array.count > 0 {
                        for i in 0...array.count-1{
                            r.append(Note(array[i]))
                        }
                    }
                    completion(r,nil)
                    return
                }
                completion(nil,"Connection Error".localized)
            case .failure(let err):
                completion(nil,"No Internet Connection".localized)
                print(err)
            }
        }
    }
    func deleteNoteWithId(_ id:String,title:String,type:Int,content:String,completion:@escaping(_ res:[Note]?,_ err:String?)->Void){
        makeRequst(params: ["id":id,"is_deleted":true,"title":title,"type":type,"content":content], completion: completion)
    }
    func editNoteWithId(_ id:String,title:String,type:Int,content:String,completion:@escaping(_ res:[Note]?,_ err:String?)->Void){
        makeRequst(params: ["id":id,"title":title,"type":type,"content":content], completion: completion)
    }
    func getNotes(completion:@escaping(_ res:[Note]?,_ err:String?)->Void){
        makeRequst(params: [:], completion: completion)
    }
    func createNoteType1(title:String,content:String,type:Int,completion:@escaping(_ res:[Note]?,_ err:String?)->Void){
        makeRequst(params:["id":UUID().uuidString.prefix(8),"title":title,"content":content,"type":type], completion: completion)
    }
}
